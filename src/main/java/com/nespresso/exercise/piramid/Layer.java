package com.nespresso.exercise.piramid;

public abstract class Layer {

	protected int stoneQantity;
	protected char stoneLayerSymbole;

	protected Layer(int stoneQantity, char stoneLayerSymbole) {

		this.stoneQantity = stoneQantity;
		this.stoneLayerSymbole = stoneLayerSymbole;
	}

	public int stoneQantity() {
		return stoneQantity;
	}

	public abstract boolean canHandleOtherLayers();

	public String print(Layer previousLayer, Layer baseOfPyramid) {
		StringBuilder layerPresentation = new StringBuilder();
		
		buildTheMainOfLayer(layerPresentation);
		
		if (this.isTheBaseLayerOfPyramid(previousLayer, baseOfPyramid)) {
			return layerPresentation.toString();
		}

		addPaddingIfNecessary(previousLayer,layerPresentation);

		addSpaceIfNecessary(baseOfPyramid,layerPresentation);

		return layerPresentation.toString();
	}

	private boolean isTheBaseLayerOfPyramid(Layer previousLayer,
			Layer baseOfPyramid) {
		return previousLayer == null && baseOfPyramid == null;
	}

	private void buildTheMainOfLayer(StringBuilder layerPresentation){
		for (int stoneIndex = 0; stoneIndex < stoneQantity; stoneIndex++) {
			layerPresentation.append(stoneLayerSymbole);
		}

	}
	
	private void addPaddingIfNecessary(Layer previousLayer ,StringBuilder layerPresentation ) {
		char BUILD_PADDING = '_';
		if (this.stoneQantity() < previousLayer.stoneQantity()) {

			int paddingQuantity = previousLayer.stoneQantity() - stoneQantity;

			for (int paddingIndex = 0; paddingIndex < paddingQuantity / 2; paddingIndex++) {
				layerPresentation.insert(0, BUILD_PADDING);
				layerPresentation.insert(layerPresentation.length(),
						BUILD_PADDING);
			}

		}
	}

	private void addSpaceIfNecessary(Layer baseOfPyramid ,StringBuilder layerPresentation ) {
		char ESPACE_PADDING = ' ';
		if (layerPresentation.length() < baseOfPyramid.stoneQantity()) {

			int spaceQuantity = baseOfPyramid.stoneQantity()
					- layerPresentation.length();

			for (int spaceIndex = 0; spaceIndex < spaceQuantity / 2; spaceIndex++) {
				layerPresentation.insert(0, ESPACE_PADDING);
				layerPresentation.insert(layerPresentation.length(),
						ESPACE_PADDING);
			}
		}
	}
}
