package com.nespresso.exercise.piramid;

public class LayerBuilder {

	private final int SLAVES_FOR_ONE_STONE = 50;
	private final int PRICE_FOR_X_STONE = 2;

	public Layer buildLayer(String layerInformation) {
		Layer newLayer = null;
		int slavesQuantity = LayerInformationParser.parseSlavesQuantity(layerInformation);
		int anksQuantity = LayerInformationParser.parseAnksBudget(layerInformation);
		int stoneQuantity = calculateStoneQuantityPerSlaves(slavesQuantity);

		LayerQuality bestBuildDecision = findBestQualityForLayer(stoneQuantity,anksQuantity);

		if (bestBuildDecision == LayerQuality.TwoAnks) {
			newLayer = new TwoAnksLayer(stoneQuantity);
		} else {
			newLayer = new OneAnksLayer(stoneQuantity);
		}

		return newLayer;
	}

	public int calculateStoneQuantityPerSlaves(int slaves) {
		return slaves / SLAVES_FOR_ONE_STONE;
	}

	private LayerQuality findBestQualityForLayer(int stoneQuantity,
			int anksBudget) {

		if (anksBudget - (stoneQuantity * PRICE_FOR_X_STONE) >= 0) {
			return LayerQuality.TwoAnks;
		} else {
			return LayerQuality.OneAnks;
		}

	}

	enum LayerQuality {
		OneAnks, TwoAnks
	}

}
