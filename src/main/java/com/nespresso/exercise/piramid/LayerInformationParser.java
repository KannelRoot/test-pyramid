package com.nespresso.exercise.piramid;

public class LayerInformationParser {


	public static int parseSlavesQuantity(String layerInformation) {
		String slavesQuantity = layerInformation.split(",")[0].split(" ")[0];
		int slavesQantityNumber = Integer.valueOf(slavesQuantity);
		return slavesQantityNumber;
	}

	public static int parseAnksBudget(String layerInformation) {
		String anksQuantity = layerInformation.split(",")[1].trim().split(" ")[0];
		int anksQantityNumber = Integer.valueOf(anksQuantity);
		return anksQantityNumber;
	}

}
