package com.nespresso.exercise.piramid;

public class OneAnksLayer extends Layer {
	private static final char STONE_SYMBOLE = 'V';

	protected OneAnksLayer(int stoneQantity) {
		super(stoneQantity, STONE_SYMBOLE);
	}

	@Override
	public boolean canHandleOtherLayers() {
		return false;
	}

}
