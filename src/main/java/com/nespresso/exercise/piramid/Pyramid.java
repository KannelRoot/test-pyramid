package com.nespresso.exercise.piramid;

import java.util.LinkedList;

public class Pyramid {

	private LinkedList<Layer> layers;
	private LayerBuilder layerCreator;
	private PyramidPresenter presenter;

	public Pyramid() {
		layers = new LinkedList<Layer>();
		layerCreator = new LayerBuilder();
		presenter = new PyramidPresenter();
	}

	public void addLayer(String layerInofrmation) {
		Layer newLayer = layerCreator.buildLayer(layerInofrmation);

		if (layers.isEmpty()) {
			layers.addFirst(newLayer);
		} else {
			buildPyramidWithRules(newLayer);
		}

	}

	private void buildPyramidWithRules(Layer newLayer) {
		Layer lastLayer = layers.getFirst();

		if (lastLayer.stoneQantity() < newLayer.stoneQantity()) {
			layers.removeFirst();
			layers.addFirst(newLayer);
		}

		if (lastLayer.stoneQantity() == newLayer.stoneQantity()
				&& lastLayer.canHandleOtherLayers()) {
			layers.addFirst(newLayer);
		}

		if (lastLayer.stoneQantity() > newLayer.stoneQantity()) {
			layers.addFirst(newLayer);
		}

	}

	public String print() {

		return presenter.presentPyramid(layers);
	}
}
