package com.nespresso.exercise.piramid;

import java.util.LinkedList;

public class PyramidPresenter {

	public String presentPyramid(final LinkedList<Layer> layers) {
		String RET_LINE = "\n";
		StringBuilder pyramidPresentation = new StringBuilder();
		int pyramidHeight = layers.size();
		Layer currentLayer, previousLayer , baseOfPyramid = layers.getLast();
		int currentLayerIndex = 0;
		for (currentLayerIndex = 0; currentLayerIndex +1 < pyramidHeight; currentLayerIndex++) {
			currentLayer = layers.get(currentLayerIndex);
			previousLayer = layers.get(currentLayerIndex + 1);
			pyramidPresentation.append(currentLayer.print(previousLayer,baseOfPyramid)+RET_LINE);
		}
		
		pyramidPresentation.append(baseOfPyramid.print(null,null));

		return pyramidPresentation.toString();
	}

}
