package com.nespresso.exercise.piramid;

public class TwoAnksLayer extends Layer {

	private static final char STONE_SYMBOLE = 'X';

	protected TwoAnksLayer(int stoneQantity) {
		super(stoneQantity, STONE_SYMBOLE);

	}

	@Override
	public boolean canHandleOtherLayers() {
		return true;
	}

}
